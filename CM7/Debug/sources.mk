################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
SIZE_OUTPUT := 
OBJDUMP_LIST := 
SU_FILES := 
EXECUTABLES := 
OBJS := 
MAP_FILES := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
AZURE_RTOS/App \
Common/Src \
Core/Src \
Core/Startup \
Core/ThreadSafe \
Drivers/STM32H7xx_HAL_Driver \
Middlewares/NetXDuo/Network/NetXDuo/Addons\ AutoIP \
Middlewares/NetXDuo/Network/NetXDuo/Addons\ Cloud \
Middlewares/NetXDuo/Network/NetXDuo/Addons\ DHCP\ Client \
Middlewares/NetXDuo/Network/NetXDuo/Addons\ DNS \
Middlewares/NetXDuo/Network/NetXDuo/Addons\ MQTT \
Middlewares/NetXDuo/Network/NetXDuo/Addons\ PPP \
Middlewares/NetXDuo/Network/NetXDuo/Addons\ SNTP \
Middlewares/NetXDuo/Network/NetXDuo/Addons\ Web\ Client \
Middlewares/NetXDuo/Network/NetXDuo/Addons\ mDNS \
Middlewares/NetXDuo/Network/NetXDuo/Crypto \
Middlewares/NetXDuo/Network/NetXDuo/NX\ Core \
Middlewares/NetXDuo/Network/NetXDuo/TLS \
Middlewares/NetXDuo/Network/NetXDuo/TraceX\ Support \
Middlewares/ThreadX/RTOS/ThreadX/Compatibility \
Middlewares/ThreadX/RTOS/ThreadX/Core \
Middlewares/ThreadX/RTOS/ThreadX/Low\ Power\ support \
Middlewares/ThreadX/RTOS/ThreadX/PerformanceInfo \
Middlewares/ThreadX/RTOS/ThreadX/TraceX\ support \
NetXDuo/App \
NetXDuo/Target \
esp-common \
esp-common/protobuf-c/protobuf-c \
host_common \
stm32/app \
stm32/app/control \
stm32/app/data \
stm32/common \
stm32/driver/network \
stm32/driver/serial \
stm32/driver/spi \

